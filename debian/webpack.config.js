'use strict';

var fs = require('fs');
var path = require('path');
var webpack = require('webpack');

var config = {

  target: 'web',
  resolve: {
    modules: ['/usr/lib/nodejs', 'debian/node_modules','node_modules'],
  },
  resolveLoader: {
    modules: ['/usr/lib/nodejs'],
  },
  node: {
    fs: 'empty'
  },
  output: {
    libraryTarget: 'umd'
  },
}

module.exports = config;
